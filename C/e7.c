/*
 * Copyright (c) YYYY Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */

#include<arpa/inet.h>
#include<netinet/in.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<regex.h>

enum status {
    HOST_UNKNOWN = -1,
    HOST_OFFLINE = 0,
    HOST_ONLINE,
};
struct host {
    char *name;
    char *domain;
    char *url;
    struct in_addr ip;
    struct in6_addr ip6;
    unsigned char mac[6];
    enum status status;
};

struct status_map{
    enum status s;
    const char *desc;
};

static struct status_map map[] = {
	{HOST_UNKNOWN,"unknown"},
	{HOST_OFFLINE,"offline"},
	{HOST_ONLINE,"online"},
};

int reg_match(char *str, int mode) {
    const char *optarg = str;
    const char *reg;
    if (str == NULL) {
        printf("Empty value\n");
        return -1;
    }

    switch (mode) {
        case 'd':
            reg = "^[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z]{2,}$";
            break;
        case '4':
            reg = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])$";
            break;
        case '6':
            reg = "^([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$";
            break;
        case 'm':
            reg = "^([0-9a-f]{2}[-:]?){6}$";
            break;
        case 'u':
            reg = "^https?:\\/\\/([a-z0-9][-a-z0-9]*\\.)+[a-z0-9][-a-z0-9]*/?";
            break;
        default:
            printf("Invalid data type!\n");
            return -1;
            break;
    }
    regex_t compreg;
    if (regcomp(&compreg, reg, REG_ICASE|REG_NOSUB|REG_EXTENDED) != 0) {
        printf("Fail to regcomp!\n");
        regfree(&compreg);
        return -1;
    }
    if (regexec(&compreg, optarg, 0, NULL, 0) != 0) {
        printf("Fail to regexec!\n");
        regfree(&compreg);
        return -1;
    }
    return 0;
}


static enum status get_status(char *status){
    struct status_map *m;
    int i;
    for(i = 0,m = map;i<sizeof(map)/sizeof(struct status_map);i++,m++){
    if(strcasecmp(m->desc, status )==0)
        return m->s;
    }
    return HOST_UNKNOWN;
}

const char *get_status_desc(enum status s){
    struct status_map *m;
    int i;
    for(i = 0,m = map;i<sizeof(map)/sizeof(struct status_map);i++,m++){
    if(m->s == s)
    return m->desc;
    }
}

int main(int agrc, char *argv[])
{
    struct host h;
    int opt;
    char buf[256];
    const char *cnm = "e:4:6:m:n:d:u:";
    char ipv4[INET_ADDRSTRLEN] = {0};
    char ipv6[INET6_ADDRSTRLEN] = {0};
    int len;
    while((opt = getopt(agrc, argv, cnm))!=-1){
        switch(opt){
        case 'e':
            h.status = get_status(optarg);
            break;
        case '4':
            if(inet_pton(AF_INET, optarg, &h.ip) !=1){
                printf("NO ipv4 addr %s\n",optarg);
                goto error;
                exit(-1);
            }
            break;
            case '6':
            if(inet_pton(AF_INET6, optarg, &h.ip6) !=1){
                printf("NO ipv6 addr %s\n",optarg);
                goto error;
                exit (-1);
            }
            break;
        case 'm':
        if((sscanf(optarg, "%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx",&h.mac[0],&h.mac[1],&h.mac[2],&h.mac[3],&h.mac[4],&h.mac[5])!=6)
        &&(sscanf(optarg, "%2hhx:%2hhx:%2hhx:%2hhx:%2hhx:%2hhx",&h.mac[0],&h.mac[1],&h.mac[2],&h.mac[3],&h.mac[4],&h.mac[5])!=6))
            {
            printf("invalid Mac %s\n", optarg);
            goto error;
            exit (-1);
            }
            break;
        case 'n':
            h.name = strdup(optarg);
            if (!h.name){
                printf("Fail to get home\n");
                goto error;
                exit (-1);
            }
            break;
        case 'd':
            h.domain = strdup(optarg);
            if (!h.domain) {
            printf("Failed to get domain\n");
            goto error;
            exit(-1);
            }
            if (reg_match(optarg, opt) != 0) {
            printf("Invalid Domain name: %s\n", optarg);
            goto error;
            exit(-1);
            }         
        break;
        case 'u':
            h.url = strdup(optarg);
            if (!h.url) {
            printf("Failed to get url\n");
            goto error;
                exit(-1);
            }
            if (reg_match(optarg, opt) != 0) {
                printf("Invalid URL: %s\n", optarg);
                goto error;
                exit(-1);
            }
            break;
    //    case '?':
        default:
            printf("usage: ./e7 -e online -4 192.168.1.1 -6 2001:0db8:85a3:0000:0000:8a2e:0370:7334 -n example.com -m 00c002123588 -u http://example.com\n");
            break;
        }
    }
    inet_ntop(AF_INET, &h.ip, ipv4, sizeof(ipv4));
    inet_ntop(AF_INET6, &h.ip6, ipv6, sizeof(ipv6));
    len = snprintf(buf, sizeof(buf), "event: %s\n",get_status_desc(h.status));
    // 
    len += snprintf(buf + len, sizeof(buf) - len, "ipv4: %s\n",ipv4);
    len += snprintf(buf + len, sizeof(buf) - len, "ipv6: %s\n",ipv6);
    len += snprintf(buf + len, sizeof(buf) - len, "mac: %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x\n", h.mac[0],h.mac[1],h.mac[2],h.mac[3],h.mac[4],h.mac[5]);
    len += snprintf(buf + len, sizeof(buf) - len, "Domain name: %s\n", h.name);
    len += snprintf(buf + len, sizeof(buf) - len, "URL: %s\n", h.url);
    printf("%s",buf);
error:
    if (h.name != NULL) free(h.name);
    return 0; 
}
