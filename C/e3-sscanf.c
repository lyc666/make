/*
 * Copyright (c) 2022 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  char buf[10];
  char *p;
  int n;
  // pass char ** for 'm' modifier e.g %ms, %m[^,']
  n = sscanf("hello, world", "%m[^,]", &p);
  if (n == 1) {
    printf("p: <%s>\n", p);
    free(p);
  } else
    printf("error modifier m\n");
  // read 12345\0
  if (sscanf("1234567890", "%5s", buf) == 1)
    printf("buff <%s>\n", buf);
  else
    printf("error modifier number\n");
  return 0;
}
