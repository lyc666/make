/*
 * Copyright (c) YYYY Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_USERS 255
#define MAX_GROUP_LEN 63
#ifdef DEBUG
#define DPRINTF(format, ...)   \
   do {printf("%s::%s "format,__FILE__,__FUNCTION__,##__VA_ARGS__ );} while (0)
#else
#define DPRINTF(format, ...)
#endif

struct user {
  char *name;
  unsigned uid;
  unsigned gid;
  char group[MAX_GROUP_LEN];
};

#define _STR(d) #d
#define STR(d) _STR(d)

int parse_group(unsigned int gid, char *group, int s)
{
  char buf[256];
  char _g_buf[MAX_GROUP_LEN+1];
  unsigned int _gid;
  FILE *gp = fopen("/etc/group","r");
  if(group == NULL) goto error;
  while (fgets(buf,sizeof(buf),gp) !=NULL){
  int n;
  n = sscanf(buf,"%" STR(MAX_GROUP_LEN) "[^:]:%*[^:]:%u:%*s\n",_g_buf,&_gid);
  if ((n==2)&&(gid== _gid)){
  DPRINTF("found %s\n", _g_buf);
  strncpy(group,_g_buf,s-1);
  group[s-1]=0;
  goto done;
   }
 }
  error:
  if (gp) fclose(gp);
  return -1;
  done:
  if (gp) fclose(gp);
  return 0;
}

int parse_user(struct user *ua, int *num)
{
  struct user *u = ua;
  char buf[256];
  FILE *passwd = fopen("/etc/passwd", "r");
  if (passwd == NULL) goto error;
  
  while (fgets(buf, sizeof(buf), passwd) != NULL) {
    int n;
    memset(u, 0, sizeof(*u));

    n = sscanf(buf, "%m[^:]:%*[^:]:%u:%u:%*[^:]:%*[^:]:%*s\n", 
               &(u->name), &(u->uid), &(u->gid));
    if (n == 3) {
      DPRINTF("%s, %u, %u\n", u->name, u->uid, u->gid);
      if (parse_group(u->gid, u->group, sizeof(u->group))) {
	    *num = u - ua;
		goto error;
      }
      if ((++u - ua) >= *num) goto quit;
      continue;
    } else {
      *num = u - ua;
      DPRINTF("parse error %d\n", n);
      goto error;
    }
  }
  *num = u - ua;
quit:
  printf("%d user(s) parsed\n", *num);
  fclose(passwd);
  return 0;
error:
  if (u->name) free(u->name);
  if (passwd) fclose(passwd);
  return -1;
}

struct user users[MAX_USERS];
void output_users(struct user users[], int num)
{
  struct user *u = users;
  for (int i = 0; i < num; i++, u++) {
    printf("-----------------\n");
    printf("Name:%s\n", u->name);
    printf("UID:%u\n",u->uid);
    printf("GID:%u\n",u->gid);
    printf("Group:%s\n",u->group);
  }
}

int main(int argc, char **argv)
{
  int nusers = sizeof(users)/sizeof(struct user);

  if ( parse_user(users, &nusers) != 0) {
    DPRINTF("%d users\n", nusers);
    return -1;
  }
  output_users(users, nusers);
}


