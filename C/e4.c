#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct user {
    char name[64];
    unsigned int uid;
    unsigned int gid;
    char home[64];
    char shell[64];
};

enum sort_type { 
    SORT_NONE,
    SORT_NAME,
    SORT_UID
};

enum sort_type sort_type = SORT_NAME;

int compare(const void *a, const void *b) {
    struct user *userA = (struct user *)a;
    struct user *userB = (struct user *)b;

    switch(sort_type) {
    case SORT_NAME:
        return strcmp(userA->name, userB->name);
    case SORT_UID:
        return (userA->uid - userB->uid);
    default:
        abort();
    }
}

int main(int argc, char *argv[]) {
    FILE *fp;
    char line[256];
    struct user users[1024];
    int user_count = 0;
    //enum sort_type sort_type;

    fp = fopen("/etc/passwd", "r");
    if (fp == NULL) {
        perror("Unable to open file");
        return(1);
    }

    while (fgets(line, sizeof(line), fp)) {
        int matches = sscanf(line, "%[^:]:%*[^:]:%u:%u:%*[^:]:%63[^:]:%63s", users[user_count].name, &users[user_count].uid, &users[user_count].gid, users[user_count].home, users[user_count].shell);
        if (matches == 3) {
            matches = sscanf(line, "%[^:]:%*[^:]:%u:%u::%63[^:]:%63s", users[user_count].name, &users[user_count].uid, &users[user_count].gid, users[user_count].home, users[user_count].shell);
        }
        if (matches != 5) {
            fprintf(stderr, "Error parsing line: %s", line);
            //printf("%d\n", matches);
            //printf("%s\n", discription);
            //free(new_user);
            continue;
        }
        user_count++;
    }

    fclose(fp);

    /*if (strcmp(argv[1], "SORT_UID") == 0) {
        sort_type = SORT_NAME;
    } else if (strcmp(argv[1], "SORT_GID") == 0) {
        sort_type = SORT_UID;
    } //  SORT_UID for sorting by uid*/

    qsort(users, user_count, sizeof(struct user), compare);

    for (int i = 0; i < user_count; i++) {
        printf("%s:%d:%d:%s:%s\n", users[i].name, users[i].uid, users[i].gid, users[i].home, users[i].shell);
    }

    return 0;
}
