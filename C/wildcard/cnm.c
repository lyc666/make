
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
pid_t wait(int *status);
int main(int argc, char const *argv[]){
    pid_t pid;
    pid=fork();
    if(pid<0)
    {
    perror("fail to fork");
    return -1;
    }
    if(pid == 0)
    {
        int i = 0;
        for(int i=0;i<5;i++)
        {
            printf("this is son process");
            sleep(1);
        }
        exit(2);
    }
    else{
        int status = 0;
        wait(&status);
        if(WIFEXITED(status)!=0)
        {
            printf("The son process return status: %d\n", WEXITSTATUS(status));

        }
        printf("this is father process\n");
    }
return 0;
}