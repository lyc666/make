/*
 * Copyright (c) YYYY Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
#define DPRINTF(format, ...)   \
   do {printf("%s::%s "format,__FILE__,__FUNCTION__,##__VA_ARGS__ );} while (0)
#else
#define DPRINTF(format, ...)
#endif

// how to convert a marco of number into const string
// e.g, STR(MAX_GROUP_LEN) is "64"
#define _STR(d) #d
#define STR(d) _STR(d)

struct user {
  char *name;
  unsigned uid;
  unsigned gid;
  char* group;
};

static int max_user = 1;

// let parse_group alloc memory
// so pass a (char *)*
int parse_group(unsigned int gid, char **group)
{
  char buf[256];
  //char _g_buf[MAX_GROUP_LEN]; not necessary now
  unsigned int _gid;
  //UT1: return -1;
  //UT2: if (gid == 3) return -1; .

  FILE *gp = fopen("/etc/group", "r");
  while (fgets(buf, sizeof(buf), gp) != NULL) {
    int n;
    // limit the size of _g_buf
    n = sscanf(buf, "%m[^:]:%*[^:]:%u:%*s\n", 
               group,  &_gid);
    if ((n == 2) && (gid == _gid)) {
      DPRINTF("found %s\n", *group);
      goto done;
    }
  }
error:
  if (gp) fclose(gp);
  return -1;
done:
  if (gp) fclose(gp);
  return 0;
}


struct user *expand_users(struct user *ua)
{
  struct user *new;
  DPRINTF("double max_user %d to %d\n", max_user, max_user *2);
  max_user *= 2;
  new = realloc(ua, sizeof(struct user) * max_user);
  if (!new) exit(-1);
  return new;
}
//user array could be realloced so need to return new us.
//num is output only parameter. caller should set initial number to 0
struct user * parse_user(struct user *ua, int *num)
{
  struct user *u = ua;	//暂存ua结构体数组指针
  char buf[256];
  FILE *passwd = fopen("/etc/passwd", "r");
  if (passwd == NULL) goto error;
  
  while (fgets(buf, sizeof(buf), passwd) != NULL) {
    int n;
    memset(u, 0, sizeof(*u));

    n = sscanf(buf, "%m[^:]:%*[^:]:%u:%u:%*[^:]:%*[^:]:%*s\n", 
               &(u->name), &(u->uid), &(u->gid));
    if (n == 3) {
      DPRINTF("%s, %u, %u\n", u->name, u->uid, u->gid);
      if (parse_group(u->gid, &u->group)) {
        // how to test?
        // 1. hardcode parse_group return -1
        // 2. hardocde parse_group return -1 when gid is 3
        *num = u - ua;
        goto error;
      }
      if ((++u - ua) >= max_user) {
        //1. expand users. 2. adjust ua, u
        ua = expand_users(ua);
        u = ua + (max_user / 2);
      }
      continue;
    } else {
      *num = u - ua;
      DPRINTF("parse error %d\n", n);
      goto error;
    }
  }
  *num = u - ua;
quit:
  printf("%d user(s) parsed\n", *num);
  fclose(passwd);
  return ua;
error:
  if (u->name) free(u->name);
  if (u->group) free(u->group);
  if (passwd) fclose(passwd);
  return ua;
}

void dump_users(struct user users[], int num)
{
  struct user *u = users;
  for (int i = 0; i < num; i++, u++) {
    printf("%s, %u, %u, %s\n", u->name, u->uid, u->gid, u->group);    
  }
}

int main(int argc, char **argv)
{
  int nusers = 0;
  struct user *users = malloc(sizeof(struct user) * max_user);
  
  users = parse_user(users, &nusers);
  DPRINTF("%d users\n", nusers);
  dump_users(users, nusers);
}
