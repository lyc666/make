/*
 * Copyright (c) 2022 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#define BUF_SIZE 1024

struct user *parse_user(char* str);

struct user {
    struct user *next;
    char* name;
    unsigned int uid;
    unsigned int gid;
    char home[64];
    char shell[64];
};

struct user *parse_user(char* str){
    struct user *new_user = (struct user*)malloc(sizeof(struct user));
    if(new_user == NULL){
       err(EXIT_FAILURE, "malloc");
    }
    memset(new_user, 0, sizeof(*new_user));

    int flags = sscanf(str, "%m[^:]:%*[^:]:%u:%u:%*[^:]:%63[^:]:%63s", \
        &(new_user->name), &(new_user->uid), &(new_user->gid), new_user->home, new_user->shell);
    int flags2 = sscanf(str, "%m[^:]:%*[^:]:%u:%u::%63[^:]:%63s", \
        &(new_user->name), &(new_user->uid), &(new_user->gid), new_user->home, new_user->shell);

    if (flags == 5 || flags2 == 5){
        printf("%s, %u, %u, %s, %s\n", \
                new_user->name, new_user->uid, new_user->gid, new_user->home, new_user->shell);
        return new_user;
    }else{
        printf("error, %d parsed\n", flags);
        printf("err str: %s\n",str);
    }

    if(new_user && new_user->name){
        free(new_user->name);
    }
    if(new_user){
        free(new_user);
    }
    return NULL;
}

int main(int argc, char* argv[]){
    FILE *fp = fopen("passwd.txt", "r");
    if(fp == NULL){
        perror("open");
        exit(EXIT_FAILURE);
    }

    char buf[BUF_SIZE];
    struct user *head = NULL;
    struct user *cur = NULL;

    while(fgets(buf, BUF_SIZE, fp)){

        struct user *newNode = parse_user(buf);
        // printf("newNode: %s", newNode->name);

        newNode->next = NULL;

        if(head == NULL){
            head = newNode;
            cur = head;
        }else{
            cur->next = newNode;
            cur = newNode;
        }

    }

    fclose(fp);

    cur = head;
    while(cur != NULL){
        printf("Name: %s\n",cur->name);
        cur = cur->next;
    }

    cur = head;
    while(cur != NULL){
        struct user *temp = cur;
        cur = cur->next;
        free(temp);
    }

    exit(EXIT_SUCCESS);
}

