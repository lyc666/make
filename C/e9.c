
/*
 * Copyright (c) 2022 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

enum event
{
    EVENT_UNDEFINED = 0x0,
    EVENT_PRESS = 0x1,
    EVENT_RELEASE = 0x2,
    EVENT_TIMEOUT = 0x4
};

enum state {
    INIT,
    BUTTON_RELASED,
    BUTTON_PRESSED,
    RESET_STAGE,
    RESTORE_STAGE,
    FIN
};

enum event read_event(unsigned int timeout,int expected_event)
{
    fd_set rfds;
    struct timeval tv;
    int retval;
    char buf[128] = {0};
redo:
    if (timeout) {
    tv.tv_sec = timeout;
    tv.tv_usec = 0;
  }
  int size;

  FD_ZERO(&rfds);
  FD_SET(0, &rfds);
  do {
    retval = select(1, &rfds, NULL, NULL, timeout?&tv:NULL);
  } while (retval == -1 && errno == EINTR);
  if ((expected_event & EVENT_TIMEOUT ) == EVENT_TIMEOUT && retval == 0)
    return EVENT_TIMEOUT;
  size = read(0, buf, sizeof(buf));
 
  if (size == 0) exit(-1);
  
  if ( (expected_event & EVENT_PRESS ) == EVENT_PRESS && strcmp(buf, "press\n") == 0)
    return EVENT_PRESS;
  else if ( (expected_event & EVENT_RELEASE ) == EVENT_RELEASE && strcmp(buf, "release\n") == 0)
    return EVENT_RELEASE;
  else
    goto redo;
}

int main(int argc, char *argv[])
{
  enum state s=INIT;
  enum event e;
  while (1) {   
    switch (s) {
      case INIT:
        printf("button system started\n");
        s = BUTTON_RELASED;
        break;
      case BUTTON_RELASED:
        printf("Button released\n");
        e = read_event(0, EVENT_PRESS);
        printf("Event %d\n", s);
        if (e == EVENT_PRESS)
          s = BUTTON_PRESSED;
        break;
      case BUTTON_PRESSED:
        printf("Button Pressed\n");
        if ((e = read_event(3, EVENT_RELEASE | EVENT_TIMEOUT)) == EVENT_RELEASE)
          s = BUTTON_RELASED;
        else if (e == EVENT_TIMEOUT)
          s = RESET_STAGE;
        break;
      case RESET_STAGE:
        printf("About to reset device\n");  
        if((e == read_event(7, EVENT_RELEASE | EVENT_TIMEOUT)) == EVENT_RELEASE)
            s = FIN;
        else if (e == EVENT_TIMEOUT)
            s = RESTORE_STAGE;
        break;

      case RESTORE_STAGE:
        printf("About to restore stage.\n");  
        e = read_event(0, EVENT_RELEASE);
        if (e == EVENT_RELEASE)
            s = FIN;
        break;
      case FIN:
        printf("Reset Device\n");
        s = BUTTON_RELASED;
        break;
      default:
        printf("Input Error\n");
        break;
    }
  }
  return 0;
}










