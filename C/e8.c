/*
 * Copyright (c) 2022 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

enum event {
  EVENT_UNDEFINED,
  EVENT_PRESS,
  EVENT_RELEASE,
  EVENT_TIMEOUT
};

// Input: 
//   timeout: 0 - no timeout; > 0: number of seconds
// Returned value:
//   read from stdin (fd is 0).
//   if input string is 'press\n' return EVENT_PRESS
//   if input string is 'release\n' return EVENT_RELEASE
//   if input string is undefined, ignore and read stdin again.
//   if nothing to read from stdin, sleep until stdin has something to read
//   or timeout
// Key lib function
//   man select
//   man read

enum event read_event(unsigned int timeout)
{
    enum event result = EVENT_UNDEFINED;
    
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);  // 将标准输入文件描述符 0 添加到文件描述符集合中
    
    struct timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = 0;
    
    int ready = select(1, &fds, NULL, NULL, &tv);  // 监听标准输入是否可读
    
    if (ready == -1) {
        perror("select");
        exit(EXIT_FAILURE);
    } else if (ready == 0) {
        result = EVENT_TIMEOUT;  // 超时事件
    } else {
        char buffer[20];
        
        if (FD_ISSET(0, &fds)) {  // 判断标准输入是否可读
            ssize_t len = read(0, buffer, sizeof(buffer));  // 从标准输入读取数据
            
            if (len == -1) {
                perror("read");
                exit(EXIT_FAILURE);
            } else if (len == 0) {
                printf("End of file\n");
                exit(EXIT_FAILURE);
            } else {
                buffer[len-1] = '\0';  // 移除换行符
                
                if (strcmp(buffer, "press") == 0) {
                    result = EVENT_PRESS;  // 按下事件
                } else if (strcmp(buffer, "release") == 0) {
                    result = EVENT_RELEASE;  // 松开事件
                }
            }
        }
    }
    
    return result;
}

int main(void)
{
    enum event current_event = EVENT_UNDEFINED;
    unsigned int timer = 0;
    
    while (1) {
        switch (current_event) {
            case EVENT_UNDEFINED:
                printf("Waiting for event...\n");
                current_event = read_event(0);
                break;
            case EVENT_PRESS:
                printf("Button Pressed\n");
                current_event = read_event(1);
                timer = 0;
                break;
            case EVENT_RELEASE:
                printf("Reboot Device\n");
                current_event = read_event(0);
                break;
            case EVENT_TIMEOUT:
                timer++;
                if (timer == 1) {
                    printf("About to reset device\n");
                } else if (timer == 10) {
                    printf("About to restore device to default\n");
                    current_event = EVENT_RELEASE;
                    timer = 0;
                }
                current_event = read_event(1);
                break;
        }
    }
    
    return 0;
}