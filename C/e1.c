/*
 * Copyright (c) YYYY Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <string.h>
int main(int argc, char *argv[]) {
    int i;

    // Check if the -n option is used
    if (argc > 1 && strcmp(argv[1], "-n") == 0) {
        // Print the string without a trailing newline
        for (i = 2; i < argc; i++) {
            printf("%s", argv[i]);
            if (i < argc - 1) {
                // Add a space between strings
                printf(" ");
            }
        }
    } else {
        // Print the string(s) with a trailing newline
        for (i = 1; i < argc; i++) {
            printf("%s", argv[i]);
            if (i < argc - 1) {
                // Add a space between strings
                printf(" ");
            }
        }
        printf("\n");
    }

    return 0;
}


