#include<arpa/inet.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

enum status {
  HOST_UNKNOWN = -1,
  HOST_OFFLINE = 0,
  HOST_ONLINE,
};
struct host {
  char *name;
  struct in_addr ip;
  struct in6_addr ip6;
  unsigned char mac[6];
  enum status status;
};

struct status_map{
 enum status s;
 const char *desc;
};

static struct status_map map[] = {
	{HOST_UNKNOWN,"unknown"},
	{HOST_OFFLINE,"offline"},
	{HOST_ONLINE,"online"},
};

static enum status get_status(char *status){
 struct status_map *m;
int i;
for(i = 0,m = map;i<sizeof(map)/sizeof(struct status_map);i++,m++){
   if(strcasecmp(m->desc, status )==0)
     return m->s;
}
return HOST_UNKNOWN;
}

const char *get_status_desc(enum status s){
 struct status_map *m;
 int i;
 for(i = 0,m = map;i<sizeof(map)/sizeof(struct status_map);i++,m++){
  if(m->s == s)
  return m->desc;
 }
}

int main(int agrc, char *argv[])
{
  struct host h;
  int opt;
  char buf[256];
  char ipv4[INET_ADDRSTRLEN] = {0};
  char ipv6[INET6_ADDRSTRLEN] = {0};
  int len;
  while((opt = getopt(agrc, argv, "e:4:6:m:n:"))!=-1){
    switch(opt){
    case 'e':
      h.status = get_status(optarg);
      break;
    case '4':
      if(inet_pton(AF_INET, optarg, &h.ip) !=1){
        printf("NO ipv4 addr %s\n",optarg);
        exit(-1);
      }
      break;
    case '6':
      if(inet_pton(AF_INET6, optarg, &h.ip6) !=1){
        printf("NO ipv6 addr %s\n",optarg);
        exit (-1);
      }
      break;
    case 'm':
      if((sscanf(optarg, "%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx",&h.mac[0],&h.mac[1],&h.mac[2],&h.mac[3],&h.mac[4],&h.mac[5])!=6)
      &&(sscanf(optarg, "%2hhx:%2hhx:%2hhx:%2hhx:%2hhx:%2hhx",&h.mac[0],&h.mac[1],&h.mac[2],&h.mac[3],&h.mac[4],&h.mac[5])!=6))
      {
      printf("invalid Mac %s\n", optarg);
      exit (-1);
      }
      break;
    case 'n':
      h.name = strdup(optarg);
      if (!h.name){
        printf("Fail to get home\n");
        exit (-1);
      }
      break;
//    case '?':
    default:
      printf("usage: host_event -e online -4 192.168.1.1 -6 fe80::2 -n mynotebook -m 00c002123588\n");
      break;
   }
  }
  inet_ntop(AF_INET, &h.ip, ipv4, sizeof(ipv4));
  inet_ntop(AF_INET6, &h.ip6, ipv6, sizeof(ipv6));
  len = snprintf(buf, sizeof(buf), "event: %s\n",get_status_desc(h.status));
  len += snprintf(buf + len, sizeof(buf) - len, "host: %s\n",h.name); 
  len += snprintf(buf + len, sizeof(buf) - len, "ipv4: %s\n",ipv4);
  len += snprintf(buf + len, sizeof(buf) - len, "ipv6: %s\n",ipv6);
  len += snprintf(buf + len, sizeof(buf) - len, "mac: %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x\n", h.mac[0],h.mac[1],h.mac[2],h.mac[3],h.mac[4],h.mac[5]);
  printf("%s",buf);
  return 0; 
}












