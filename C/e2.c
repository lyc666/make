/*
 * Copyright (c) 2023 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  int ch ;
  int bflag,nflag,eflag,vflag,fflag,flag,sflag,tflag;
  long int bsize;
  while ((ch = getopt(argc, argv, "B:beflnstuv")) != -1) {
  switch (ch) {
    case 'B':
      bsize = (size_t)strtol(optarg, NULL, 0);
      break;
    case 'b':
      bflag = nflag = 1;	/* -b implies -n */
      break;
    case 'e':
      eflag = vflag = 1;	/* -e implies -v */
      break;
    case 'f':
      fflag = 1;
      break;
    case 'l':
      flag = 1;
      break;
    case 'n':
      nflag = 1;
      break;
    case 's':
      sflag = 1;
      break;
    case 't':
      tflag = vflag = 1;	/* -t implies -v */
      break;
    case 'u':
      setbuf(stdout, NULL);
      break;
    case 'v':
      int vflag = 1;
      break;
    default:
    case '?':
      (void)fprintf(stderr,
                    "Usage: %s [-beflnstuv] [-B bsize] [-] "
                    "[file ...]\n", getprogname());
      return EXIT_FAILURE;
  }
}
}
