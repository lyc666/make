/*
 * Copyright (c) 2022 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
// include files 
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define STR_FOG "fog:x:1003:1003:,,,:/home/fog:/bin/bash"
struct user {
  char *name;
  unsigned int uid;
  unsigned int gid;
  char home[64];
  char shell[64];
};

struct user *parse_user(char* str)
{
int n;
struct user *u = malloc(sizeof(struct user));

if(!u) {
 printf("malloc failed\n");
 exit(-1);
}
memset(u,0,sizeof(*u));
n = sscanf(str, "%m[^:]:%*[^:]:%u:%u:%*[^:]:%63[^:]:%63s", &(u->name), &(u->uid), &(u->gid), u->home, u->shell);
if(n==5) {
printf("%s.%u,%u,%s,%s\n",u->name,u->uid,u->gid,u->home,u->shell);
	return u;
}else
if (u && u->name) free(u->name);
if (u) free(u);
return NULL;
}

int main(int argc, char **argv)
{
char buf[10];
char buff[256];
FILE *fp;
char *openfile= "/etc/passwd";
if((fp = fopen(openfile, "r"))==NULL){
   printf("open file error\n");
   exit(EXIT_FAILURE);
}
   while(fgets(buff,256,fp)){
        struct user *u = parse_user(buff);
    out:
        if (u && u->name) free(u->name);
        if (u) free(u);
   }
 fclose(fp);
return 0;
}













