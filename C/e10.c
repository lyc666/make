/*
 * Copyright (c) 2022 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit.
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */

#ifdef DEBUG
#define DPRINTF(format, ...)                                              \
    do {                                                                  \
        printf("%s: :%s " format, __FILE__, __FUNCTION__, ##__VA_ARGS__); \
    } while (0)
#define DCONSOLE(format, ...)                                                     \
    do {                                                                          \
        FILE *fp;                                                                 \
        if ((fp = fopen("/dev/console", "w")) != NULL) {                          \
            fprintf(fp, "%s::%s " format, __FILE__, __FUNCTION__, ##__VA_ARGS__); \
            fclose(fp);                                                           \
        }                                                                         \
    } while (0)
#else
#define DPRINTF(format, ...)
#define DCONSOLE(format, ...)
#endif

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

enum event
{
    EVENT_UNDEFINED,
    EVENT_PRESS = 0x1,
    EVENT_RELEASE = 0x2,
    EVENT_TIMEOUT = 0x4
};
enum state
{
    INIT,
    BUTTON_RELASED,
    BUTTON_PRESSED,
    RESET_STAGE,
    RESTORE_STAGE,
    FIN
};

const static char *e_name[8] = {"undefined",
                                "press",
                                "release",
                                "press || release",
                                "timeout",
                                "press || timeout",
                                "release || timeout",
                                "press || release || timeout"};

// Input:
//   timeout: 0 - no timeout; > 0: number of seconds
// Returned value:
//   read from stdin (fd is 0).
//   if input string is 'press\n' return EVENT_PRESS
//   if input string is 'release\n' return EVENT_RELEASE
//   if input string is undefined, ignore and read stdin again.
//   if nothing to read from stdin, sleep until stdin has something to read
//   or timeout
// Key lib function
//   man select
//   man read

enum event read_event(unsigned int timeout, int expected_event)
{
    fd_set set;
    struct timeval tv;
    char buf[128] = {0};
    int reval;
    enum event e;
    FD_SET(STDIN_FILENO, &set);

    tv.tv_sec = timeout;
    tv.tv_usec = 0;

    while (1) {
        memset(buf, 0, 128);
        reval = select(1, &set, NULL, NULL, timeout ? &tv : NULL);
        if (reval == -1) continue;
        if (reval == 0)
            return EVENT_TIMEOUT;
        else if (read(STDIN_FILENO, buf, sizeof(buf)) != -1) {
            DPRINTF("input is %s\n",buf);
            if (strcmp(buf, "press\n") == 0)
                e = EVENT_PRESS;
            else if (strcmp(buf, "release\n") == 0)
                e = EVENT_RELEASE;
            else
                e = EVENT_UNDEFINED;
        }
        DPRINTF("e:%d,exp:%d,e&exp:%d", e, expected_event, e & expected_event);
        if ((e & expected_event) != 0) {
        return e;
        } else
           DPRINTF("ERROR INPUT: %sEXPECTED INPUT IS: %s\n", buf,
                   e_name[expected_event]);
    }
}

int main(int argc, char const *argv[])
{
    enum state s = INIT;
    enum event e;
    while (1) {
        switch (s) {
        case INIT: s = BUTTON_RELASED; break;
        case BUTTON_RELASED:
            e = read_event(0, EVENT_PRESS);
            s = BUTTON_PRESSED;
            break;
        case BUTTON_PRESSED:
            printf("Button Pressed \n");
            e = read_event(3, (EVENT_RELEASE | EVENT_TIMEOUT));
            if (e == EVENT_RELEASE) {
                s = BUTTON_RELASED;
                printf("Button release \n");
            } else if (e == EVENT_TIMEOUT)
                s = RESET_STAGE;
            break;
        case RESET_STAGE:
            printf("About to reset device \n");
            e = read_event(7, (EVENT_RELEASE | EVENT_TIMEOUT));
            if (e == EVENT_RELEASE)
                s = FIN;
            else if (e == EVENT_TIMEOUT)
                s = RESTORE_STAGE;
            break;
        case RESTORE_STAGE:
            printf("About to restore device to default \n");
            e = read_event(0, EVENT_RELEASE);
            s = FIN;
            break;
        case FIN:
            printf("Reboot Device \n");
            s = BUTTON_RELASED;
            break;
        default: break;
        }
    }
    exit (-1);
    return 0;
}







