#include <sys/types.h>
#include <regex.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int reg_match(char *arg, int mode)
{
    const char * pregexstr;
    const char * ptext = arg;
    regex_t oregex;
    int status = 0;    
   
    if(mode == 1){
        pregexstr = "^https?:\\/\\/([a-z0-9][-a-z0-9]*\\.)+[a-z0-9][-a-z0-9]*/?";       //URL 
    }
    else if(mode == 2){
        pregexstr =  "^[a-f0-9]{2}(([:-])?[a-f0-9]{2}){5}$";        					//MAC addr
    }
    else {
        pregexstr =  "^([a-z0-9][-a-z0-9]*\\.)+[a-z0-9][-a-z0-9]*";        				//Domain name	
    }
    

    if ((status = regcomp(&oregex, pregexstr, REG_ICASE|REG_NOSUB|REG_EXTENDED)) == 0) {
        status = regexec(&oregex, ptext, 0, NULL, 0);
	    if (status == 0) {
            printf("%s matches %s\n", ptext, pregexstr);
            regfree(&oregex);
            return 0;
        } 
	    else if (status == REG_NOMATCH){
	        printf("no match\n");
	         return -1;
	    }
	    else {
    	    printf("regexec error\n");
	        return -1;
   	    }
    }
    else
    {
    	printf("regcomp error\n");
	    return -1;
    
    }

}
    
int main(int argc, char **argv)
{
    char ch;
    int mode = 0;
    
    while ((ch = getopt(argc, argv, "u:m:d:")) != -1) {
     switch (ch) {
        case 'u':
            mode = 1;
            printf("The URL is %s\n",optarg);
            reg_match(optarg, mode);
            break;
        case 'm':
            mode = 2;
            printf("The mac addr is %s\n",optarg);
            reg_match(optarg, mode);
	    break;
	case 'd':
            mode = 3;
            printf("The domain name is %s\n",optarg);
            reg_match(optarg, mode);
	    break;
        default:
        case '?':
            printf("Argument error!\n");                
            exit(1);          
     }
    }
    


    return 0;
}

