/*
 * Copyright (c) YYYY Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct process {
    char command[8];
    int pid;
    int ppid;
};

void print_process_info(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Unable to open file");
        return;
    }

    char line[256];
    struct process process;

    while (fgets(line, sizeof(line), file)) {
        if (sscanf(line, "Name: %s", process.command) == 1) {
            printf("command: %s\n", process.command);
        } else if (sscanf(line, "Pid: %d", &process.pid) == 1) {
            printf("Pid: %d\n", process.pid);
        } else if (sscanf(line, "PPid: %d", &process.ppid) == 1) {
            printf("ppid: %d\n", process.ppid);
        }
    }

    printf("----------------------\n");

    char parent_filename[32];
    sprintf(parent_filename, "/proc/%d/status", process.ppid);
    if (process.ppid != 0) {
        print_process_info(parent_filename);
    }
    fclose(file);
}

int main() {
    print_process_info("/proc/self/status");
    return 0;
}
