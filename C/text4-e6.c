#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

enum status {
  HOST_UNKNOWN = -1,
  HOST_OFFLINE = 0,
  HOST_ONLINE,
};

struct host {
  char *name;
  struct in_addr ip;
  struct in6_addr ip6;
  unsigned char mac[6];
  enum status status;
};

void convert_to_string(char *output, const char *event, const char *ipv4, const char *ipv6, const char *mac) {
  sprintf(output, "event: %s\nIPv4: %s, IPv6: %s, MAC: %s", event, ipv4, ipv6, mac);
}

void convert_from_string(const char *input, struct host *host) {
  const char *ptr = input;
  
  // Parsing event
  ptr = strstr(input, "-e");
  if (ptr == NULL)
    return;
  ptr += 3; // Skip "-e "
  if (strncmp(ptr, "online", 6) == 0) {
    host->status = HOST_ONLINE;
  } else if (strncmp(ptr, "offline", 7) == 0) {
    host->status = HOST_OFFLINE;
  } else {
    host->status = HOST_UNKNOWN;
    return;
  }

  // Parsing IPv4
  ptr = strstr(input, "-4");
  if (ptr == NULL)
    return;
  ptr += 3; // Skip "-4 "
  if (inet_aton(ptr, &(host->ip)) == 0)
    return;

  // Parsing IPv6
  ptr = strstr(input, "-6");
  if (ptr == NULL)
    return;
  ptr += 3; // Skip "-6 "
  if (inet_pton(AF_INET6, ptr, &(host->ip6)) != 1)
    return;

  // Parsing name
  ptr = strstr(input, "-n");
  if (ptr == NULL)
    return;
  ptr += 3; // Skip "-n "
  host->name = strdup(ptr);

  // Parsing MAC address
  ptr = strstr(input, "-m");
  if (ptr == NULL)
    return;
  ptr += 3; // Skip "-m "
  if (sscanf(ptr, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
             &(host->mac[0]), &(host->mac[1]), &(host->mac[2]),
             &(host->mac[3]), &(host->mac[4]), &(host->mac[5])) != 6)
    return;
}

int main(int argc, char *argv[]) {
  if (argc == 10) {
    char output[1024];

    convert_to_string(output, argv[2], argv[4], argv[6], argv[8]);
    printf("%s\n", output);

    struct host myHost;
    convert_from_string(argv[1], &myHost);
    printf("Event: %d\n", myHost.status);
    printf("IPv4: %s\n", inet_ntoa(myHost.ip));
    char ipv6[INET6_ADDRSTRLEN];
    printf("IPv6: %s\n", inet_ntop(AF_INET6, &(myHost.ip6), ipv6, INET6_ADDRSTRLEN));
    printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X\n", myHost.mac[0], myHost.mac[1], myHost.mac[2],
                                                    myHost.mac[3], myHost.mac[4], myHost.mac[5]);
  } else {
    printf("Invalid command line arguments\n");
    return 1;
  }

  return 0;
}

