#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_USERS 255
#define MAX_GROUP_LEN 64

struct user {
    char *name;
    unsigned int uid;
    unsigned int gid;
    char group[MAX_GROUP_LEN];
};

struct user* parse_passwd(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return NULL;
    }

    struct user* users = (struct user*)malloc(MAX_USERS * sizeof(struct user));
    if (users == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return NULL;
    }

    char line[256];
    int count = 0;
    while (fgets(line, sizeof(line), file)) {
        if (count >= MAX_USERS) {
            printf("Maximum number of users exceeded\n");
            goto error;
        }

        struct user* current_user = &users[count];

        char name[256];
        unsigned int uid, gid;
        char group[MAX_GROUP_LEN];

        if (sscanf(line, "%255[^:]:%*[^:]:%u:%u:%*[^:]:%*[^:]:%255s", name, &uid, &gid, group) == 4) {
            current_user->name = strdup(name);
            current_user->uid = uid;
            current_user->gid = gid;
            strncpy(current_user->group, group, sizeof(current_user->group));
            count++;
        }
    }

    fclose(file);
    return users;

error:
    fclose(file);
    for (int i = 0; i < count; i++) {
        free(users[i].name);
    }
    free(users);
    return NULL;
}

char* parse_group(const char* filename, unsigned int gid) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return NULL;    }

    char line[256];
    while (fgets(line, sizeof(line), file)) {
        char name[256];
        unsigned int current_gid;

        if (sscanf(line, "%255[^:]:%*[^:]:%u", name, &current_gid) == 2 && current_gid == gid) {
            fclose(file);
            return strdup(name);
        }
    }

    fclose(file);
    return NULL;
}

int main() {
    struct user* passwd_users = parse_passwd("/etc/passwd");
    if (passwd_users != NULL) {
        // Do something with passwd_users array
        // For example, print the users' information
        for (int i = 0; i < MAX_USERS; i++) {
            if (passwd_users[i].name != NULL) {
                printf("Name: %s\n", passwd_users[i].name);
                printf("UID: %u\n", passwd_users[i].uid);
                printf("GID: %u\n", passwd_users[i].gid);
                printf("Group: %s\n", passwd_users[i].group);
                printf("\n");
            }
        }

        // Free the allocated memory
        for (int i = 0; i < MAX_USERS; i++) {
            free(passwd_users[i].name);
        }
        free(passwd_users);
    }

    char* group_name = parse_group("/etc/group", 1001);  // Replace 1001 with the desired GID
    if (group_name != NULL) {
        printf("Group Name: %s\n", group_name);
        free(group_name);
    }

    return 0;
}

