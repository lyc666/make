/*
 * Copyright (c) 2022 Sercomm Corporation. All Rights Reserved.
 *
 * Sercomm Corporation reserves the right to make changes to this document
 * without notice. Sercomm Corporation makes no warranty, representation or
 * guarantee regarding the suitability of its products for any particular
 * purpose.
 *
 * Sercomm Corporation assumes no liability arising out of the application or
 * use of any product or circuit. 
 *
 * Sercomm Corporation specifically disclaims all liability, including
 * without limitation, consequential or incidental damages; neither does it convey
 * any license under its patent rights, nor the rights of others.
 */
// include files 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// define struct

struct user {
  char *name;
  unsigned int uid;
  unsigned int gid;
  char home[64];
  char shell[64];
};

// define global variable
struct user users[256];

// function paradigm 1
int parse_user(char *str, struct user *u)
{
  memset(u, 0, sizeof(*u));
  //Paradigm: if (sscanf(...) == number) {...} else if (sscanf(...) == number) {...} ...
  if (sscanf(str, "%m[^:]:%*[^:]:%u:%u:%*[^:]:%63[^:]:%63s", 
             &(u->name), &(u->uid), &(u->gid), u->home, u->shell) == 5) {
    return 0;
  } else if (sscanf(str, "%m[^:]:%*[^:]:%u:%u::%63[^:]:%63s", 
                    &(u->name), &(u->uid), &(u->gid), u->home, u->shell) == 5) {
    return 0;
  }           
  return -1;
}

int compare_name(const void *d1, const void *d2)
{
  return strcasecmp(((struct user *)d1)->name, ((struct user *)d2)->name);
}

int compare_uid(const void*d1, const void*d2)
{
  return ((struct user *)d1)->uid - ((struct user *)d2)->uid;
}

int main(int argc, char *argv[])
{
  int nflag, uflag, user_count = 0, i;
  char buff[256];
  FILE *fp;
  if( argc > 2){
    printf("Invalid command line parameter\n");
    return 0;
  }
  if(argc == 2){
    if((strcmp(argv[1],"-n") == 0)){ //nflag and uflag are exclude.
      nflag = 1;
      uflag = 0;
    }else if(strcmp(argv[1],"-u") == 0){
      uflag = 1;
      nflag = 0;
    }else{
      printf("Invalid command line parameter:%s\n",argv[1]);
      return 0;
    }
  }
  
  if ((fp = fopen("/etc/passwd", "r")) == NULL){
    printf("open file error\n");
    exit(EXIT_FAILURE);
  } 
  while (fgets(buff,256,fp)){
    if (parse_user(buff, &users[user_count++])) {
      printf("Parse users error\n");
      exit(-1);
    }
  }
  fclose(fp);

  if (nflag)
    qsort(users, user_count, sizeof(struct user) ,compare_name);
  else if (uflag)
    qsort(users, user_count, sizeof(struct user) ,compare_uid);

  for (i = 0; i < user_count; i++)
    printf("%s %u %u %s %s\n",users[i].name
           ,users[i].uid
           ,users[i].gid
           ,users[i].home
           ,users[i].shell);
  // clean up memory
  for (i = 0; i < user_count; i++) {
    if (users[i].name) free(users[i].name);
  }
  return 0;
}
