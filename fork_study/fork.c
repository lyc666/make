#include <stdio.h>
    #include <unistd.h>

    int main(int argc,char *argv[])
    {
        printf("hello!\n");

        fork();//  --> 产生一个新的子进程，至于父子进程谁先谁后，那就是随机的！

        //从这句话开始，父子进程同时执行以下的代码：
        printf("world!\n");

        return 0;
    }
