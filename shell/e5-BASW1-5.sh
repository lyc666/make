#!/bin/bash

# 使用who命令获取所有已登录用户的用户名和终端信息
logged_in_users=$(who | awk '{print $1}')

# 遍历已登录的用户
for user in $logged_in_users
do
    # 使用getent命令获取用户的主目录路径
    home_directory=$(getent passwd $user | awk -F ':' '{print $6}')

    # 使用lastlog命令获取用户的上次登录时间和日期
    last_login=$(lastlog -u $user | awk 'NR == 2 {print $4, $5}')

    # 打印用户的信息
    echo "User: $user"
    echo "Home Directory: $home_directory"
    echo "Last Login: $last_login"
done

