#!/bin/bash

# 生成一个8个字符的伪随机密码
generate_password() {
    # 定义包含数字和字母的字符集合
    characters="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    password=""
    digit_count=0

    # 循环8次，生成一个8个字符的密码
    for ((i = 0; i < 8; i++))
    do
        # 随机选择一个字符
        random_character=${characters:RANDOM%${#characters}:1}
        
        # 如果选择的字符是数字，则增加数字计数器
        if [[ "$random_character" =~ [0-9] ]]
        then
            ((digit_count++))
        fi

        # 将选择的字符添加到密码中
        password="$password$random_character"
    done

    # 如果数字计数器少于2，则递归调用函数重新生成密码
    if ((digit_count < 2))
    then
        generate_password
    else
        echo $password
    fi
}

# 生成密码
password=$(generate_password)

# 打印密码
echo "Generated Password: $password"

