#!/bin/bash

# 定义一个变量
my_var="Hello world"

# 导出变量
export my_var

# 在同一个shell会话中访问导出的变量
echo $my_var
#echo $1
#echo $0
echo $_
echo $$
echo $?
