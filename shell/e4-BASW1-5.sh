#!/bin/bash

#生成1~100的随机数
random_number=$((RANDOM % 100+1))
#游戏循环
if (($((random_number%2))==0))
then 
   echo "the number is odd"
else
   echo "the number is even"
fi
if (($((random_number%3))==0))
then
   echo "the number can be divided by 3"
else
   echo "the number can not be divided by 3"	
fi
while true
do
   #打印猜测提示
   echo "guess a number between 1 and 100"
#   echo " "
  #用户输入猜测的数字
   read -p "Your guess: " guess

   if [[ $guess =~ ^[1-9][0-9]?$|^100$ ]]; then
      echo "输入的数在1~100之间"
   else
      echo "输入的数不在1~100之间"
   fi
  #检查用户猜测的数字与生成的随机数之间的关系
   if [ $guess -eq $random_number ]; then
        echo "Congratulations! You guessed the correct number ($random_number)."
        break
   elif [ $guess -gt $random_number ]; then
        echo "Try guessing a lower number."
   elif [ $guess -lt $random_number ]; then
        echo "Try guessing a higher number."
   fi

   echo ""
done
