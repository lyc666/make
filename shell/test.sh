#!/bin/bash
# for-loopcmd.sh: for-loop with [list]
#+ generated by command substitution.
#NUMBERS="9 7 3 8 37.53"
#echo `expr "$stringZ" : '.*'` # 15for number in `echo $NUMBERS` # for number in 9 7 3 8 37.53
#do
#echo -n "$number "
#done

stringZ=abcABC123ABCabc
echo ${#stringZ} # 15
echo `expr length $stringZ` # 15
echo `expr "$stringZ" : '.*'` # 15
echo $?

stringZ=abcABC123ABCabc
# |------|
# 12345678
echo `expr match "$stringZ" 'abc[A-Z]*.2'` # 8
echo `expr "$stringZ" : 'abc[A-Z]*.2'` # 8

# Rename all filenames in $PWD with "TXT" suffix to a "txt" suffix.
# For example, "file1.TXT" becomes "file1.txt" . . .
SUFF=TXT
suff=txt
for i in $(ls *.$SUFF)
do
mv -f $i ${i%.$SUFF}.$suff
# Leave unchanged everything *except* the shortest pattern match
#+ starting from the right-hand-side of the variable $i . . .
done ### This could be condensed into a "one-liner" if desired.
# Thank you, Rory Winston

random_number=$RANDOM
echo "Random number: $random_number"

random_number=$(dd if=/dev/urandom bs=4 count=1 status=none | od -A n -t u4)
echo "Random number: $random_number"

random_number=$(hexdump -n 4 -e '1/4 "%u"' /dev/urandom)
echo "Random number: $random_number"

random_number=$(od -A n -N 4 -tu4 /dev/urandom | awk '{print $1}')
echo "Random number: $random_number"


























