#!/bin/bash
for cnm in 1 10 100 1000
do
if (($cnm==1));then
{
    min=0
    max=1024
    num=1k
}
elif (($cnm==10));then
{
    min=1024
    max=10240
    num=10k
}
elif (($cnm==100));then
{
    min=10240
    max=102400
    num=100k
}
else (($cnm==1000))
{
    min=102400
    max=1048576
    num=1m
}
fi
echo `find linux-4.14.292/include/linux -type f -size +${min}c -size -${max}c -exec zip ${num}.zip {} \;`
done
