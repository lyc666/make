#!/bin/bash
 for KB in 1 10 100 1000
do
   if (($KB==1)); then
   {
         min=0
         max=1024
         yes=1k
   }
   elif (($KB==10)); then
   {
         min=1024
         max=10240
         yes=10k
    }
   elif (($KB==100)); then
    {
         min=10240
         max=102400
         yes=100k
    }
   else
    {
         min=102400
         max=1048576
         yes=1m
    }
   fi
find linux-4.14.292/include/linux -type f -size +${min}c -size -${max}c | zip -@ ${yes}.zip 
done
