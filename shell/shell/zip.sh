#!/bin/bash

for max in 1 10 100 1000
do
   
    if [ $max = 1 ]; then
	 echo `find linux-4.14.292/include/linux -type f -size -${max}k -exec zip ${max}k.zip {} \;`
    elif [ $max = 1000 ]; then
	 echo `find linux-4.14.292/include/linux -type f -size +${min}k -size -${max}k -exec zip 1m.zip {} \;`
    else
	 min=$((max/10))
    	 echo `find linux-4.14.292/include/linux -type f -size +${min}k -size -${max}k -exec zip ${max}k.zip {} \;`
    fi
    
done

exit 0
