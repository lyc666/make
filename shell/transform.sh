#!/bin/bash

function print_args {
  echo "Arguments using \$@:"
  for arg in "$@"; do
    echo "$arg"
  done
}

print_args "apple" "banana" "cherry"
