
#!/bin/bash
#编写一个脚本，批量压缩指定目录下的所有文件为.tar.gz格式的压缩包
# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供目录路径作为参数"
    exit 1
fi

# 检查目录是否存在
if [ ! -d "$1" ]; then
    echo "指定目录不存在"
    exit 1
fi

# 提取传递的目录路径
directory="$1"

# 输出压缩包的名称
archive_name=$(basename "$directory").tar.gz

# 创建压缩包
tar -czf "$archive_name" "$directory"
echo "压缩包创建完毕：$archive_name"
