#编写一个脚本，接受一个文件作为输入，并输出该文件中出现频率最高的单词和它的频率。
#!/bin/bash

# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供一个文件作为参数"
    exit 1
fi

# 检查文件是否存在
if [ ! -f "$1" ]; then
    echo "指定的文件不存在"
    exit 1
fi

# 保存文件路径为变量
file="$1"

# 使用 awk 命令统计单词频率，并将结果排序
result=$(awk '{ for (i=1; i<=NF; i++) count[$i]++ } END { for (word in count) print word, count[word] }' "$file" | sort -nr -k2)

# 获取频率最高的单词及其频率
most_frequent_word=$(echo "$result" | head -1)

# 分割单词和频率
word=$(echo "$most_frequent_word" | awk '{print $1}')
frequency=$(echo "$most_frequent_word" | awk '{print $2}')

# 输出结果
echo "文件中出现频率最高的单词是: $word"
echo "它出现的频率是: $frequency"

