#!/bin/bash
#编写一个脚本，接受一个数字作为参数，并判断该数字是否为质数。
# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供一个数字作为参数"
    exit 1
fi

# 提取传递的数字
number="$1"

# 判断是否为质数的函数
is_prime() {
    local num=$1
    if [ $num -lt 2 ]; then
        return 1
    fi
    for (( i=2; i*i <= num; i++ )); do
        if (( num % i == 0 )); then
            return 1
        fi
    done
    return 0
}

# 调用函数进行判断
is_prime "$number"

# 根据返回值判断结果
if [ $? -eq 0 ]; then
    echo "$number 是一个质数"
else
    echo "$number 不是一个质数"
fi

