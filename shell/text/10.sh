#编写一个脚本，接受一个目录作为参数，并将该目录下的所有文件按大小进行排序并输出到一个新文件中。
#!/bin/bash

# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供一个目录路径作为参数"
    exit 1
fi

# 检查目录是否存在
if [ ! -d "$1" ]; then
    echo "指定的目录不存在"
    exit 1
fi

# 提取传递的目录路径
directory="$1"

# 生成临时文件用于保存排序后的结果
temp_file=$(mktemp)

# 使用 find 命令查找目录下的所有文件并按大小排序，将结果保存到临时文件中
find "$directory" -type f -exec ls -lS {} + > "$temp_file"

# 检查是否找到文件
lines=$(cat "$temp_file" | wc -l)
if [ "$lines" -eq 0 ]; then
    echo "未找到任何文件"
    rm "$temp_file"
    exit 0
fi

# 将排序后的结果输出到新文件中
sorted_file="sorted_files.txt"
cat "$temp_file" > "$sorted_file"

echo "已将目录 $directory 下的所有文件按大小排序并保存到 $sorted_file 文件中"

# 删除临时文件
rm "$temp_file"

