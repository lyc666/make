#!/bin/bash
#编写一个脚本，接受一个目录作为参数，并删除该目录下的所有空文件和空文件夹。
# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供一个目录路径作为参数"
    exit 1
fi

# 检查目录是否存在
if [ ! -d "$1" ]; then
    echo "指定的目录不存在"
    exit 1
fi

# 提取传递的目录路径
directory="$1"

# 使用 find 命令查找并删除所有空文件和空文件夹
find "$directory" -type f -empty -delete
find "$directory" -type d -empty -delete

echo "已删除目录 $directory 下的所有空文件和空文件夹"

