#!/bin/bash
#编写一个脚本，接受一个目录作为参数，并批量重命名该目录下的所有文件，将文件名中的空格替换为下划线
# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供目录路径作为参数"
    exit 1
fi

# 检查目录是否存在
if [ ! -d "$1" ]; then
    echo "指定目录不存在"
    exit 1
fi

# 进入指定目录
cd "$1"

# 遍历目录下所有文件
for file in *; do
    # 判断文件名是否包含空格
    if [[ "$file" == *" "* ]]; then
        # 使用下划线替换文件名中的空格
        new_name="${file// /_}"
        # 重命名文件
        mv "$file" "$new_name"
        echo "重命名文件 '$file' 为 '$new_name'"
    fi
done
