#!/bin/bash
#编写一个脚本，接受一个目录作为参数，并统计该目录下文件的数量。
#检查参数是否为空
read -p "输入目录:" dir
if [ -z "$dir" ]; then
    echo "请提供目录路径作为参数"
    exit 1
fi
# 检查目录是否存在
if [ ! -d "$dir" ]; then
    echo "指定目录不存在"
    exit 1
fi
#统计目录下的文件数量
file_count=$(ls -l "$dir" | wc -l)
echo "指定文件夹'$dir'下的文件数量为：$file_count"