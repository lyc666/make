#!/bin/bash
#编写一个脚本，接受一个文件作为参数，并计算该文件中包含的行数。
#检查是否传递文件名作为参数
if [ $# -eq 0 ]; then
  echo "请提供一个文件名作为参数"
  exit 1
fi

# 保存文件名为变量
file="$1"

# 使用 wc 命令计算文件的行数
line_count=$(wc -l < "$file")

echo "文件 $file 中的行数是: $line_count"
