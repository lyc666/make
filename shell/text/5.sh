#!/bin/bash
#编写一个脚本，在一个文件中查找特定的字符串，并输出包含该字符串的行以及对应的行号。

# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供文件路径作为参数"
    exit 1
fi

# 检查文件是否存在
if [ ! -f "$1" ]; then
    echo "指定文件不存在"
    exit 1
fi

# 保存文件路径为变量
file="$1"
search_string="特定字符串"  # 替换为你想要查找的字符串

# 使用 grep 命令在文件中查找字符串，并输出包含字符串的行以及行号
grep -n "$search_string" "$file"

