#编写一个脚本，接受一个数字作为参数，并输出该数字的阶乘。i
#!/bin/bash

# 检查参数是否为空
if [ -z "$1" ]; then
    echo "请提供一个数字作为参数"
    exit 1
fi

# 提取传递的数字
number="$1"

# 检查数字是否为正整数
if ! [[ $number =~ ^[0-9]+$ ]] || [ "$number" -lt 0 ]; then
    echo "请提供一个正整数作为参数"
    exit 1
fi

# 定义阶乘函数
factorial() {
    if [ "$1" -le 1 ]; then
        echo 1
    else
        echo $(( $1 * $(factorial $(( $1 - 1 ))) ))
    fi
}

# 调用阶乘函数并输出结果
result=$(factorial "$number")
echo "$number 的阶乘是: $result"

