#!/bin/bash

characters="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
password=""

# 生成至少两个数字的密码
while [ $(echo "$password" | grep -o [0-9] | wc -l) -lt 2 ]
do
    password=""
    for i in {1..8}
    do
        index=$((RANDOM % ${#characters}))
        password+=${characters:index:1}
    done
done

echo $password
