#!/bin/bash
is_prime() {
    number=$1
    if [ $number -lt 2 ]; then
        return 1
    fi
    for ((i=2; i*i<=$number; i++)); do
        if [ $((number%i)) -eq 0 ]; then
            return 1
        fi
    done
    return 0
}

for ((number=1; number<=1000; number++)); do
    if is_prime $number; then
        echo $number
    fi
done
